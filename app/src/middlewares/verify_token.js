 const config = require('../config');
 const jwt = require('jsonwebtoken');
 const verifyTokens = (req,res,next) =>{
        //Get the auth header value...
        const bearerHeader = req.headers['authorization'];
        // Check if bearer in undefined..
        if(typeof bearerHeader !== 'undefined'){
            // Split at the space..
            const bearer = bearerHeader.split(' ');
            const token = bearer[1];
            if (token) {
                jwt.verify(token, config.secret, (err, decoded) => {
                    if (err) {
                        return res.json({
                            success: false,
                            message: 'Token is not valid'
                        });
                    } else {
                        req.decoded = decoded;
                        req.token = token;
                        next();
                    }
                });
            } else {
                //Forbidden..
                res.status(403).send('you are not authorised !');
            }

        }else{
            //Forbidden..
            res.status(403).send('you are not authorised !');
        }
};
module.exports = verifyTokens;