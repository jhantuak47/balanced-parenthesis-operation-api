const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    username:String,
    email: {
        type: String,
        required: [true, 'email is required'],
        unique: true
    },
    password:{
        type:String,
        required: [true,'please provide valid password'],
    },
    dob: { type: Date , required: [true, 'Date of birth must be provided']},
    role: {
        type:Boolean,
        default:false //for user..
    },
});
let User = mongoose.model('User', UserSchema);
module.exports = User;