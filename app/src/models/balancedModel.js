const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let balancedSchema = new Schema({
    user:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    message:{
        type:String,
        default:'',
    },
    attempts:{
        type:Number,
        default:0
    }
}, { collection: 'balanced' });
balancedSchema.statics.findAndModify = function (query, sort, doc, options, callback) {
    return this.collection.findAndModify(query, sort, doc, options, callback);
};
let balanced = mongoose.model('Balanced', balancedSchema);
module.exports = balanced;