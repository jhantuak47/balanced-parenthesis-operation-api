let express = require('express');
let config = require('../config');
let jwt = require('jsonwebtoken');
let router = express.Router();
let AuthModel = require('../models/authModel');

// middleware that is specific to this router
router.use(function timeLog (req, res, next) {
    console.log('Time: ', Date.now());
    next()
});
// Registration..
router.post('/register',(req,res) =>{
    //req.body
    if(!req.body){
        return res.status(400).send('Request Body is missing !!');
    }
    let Auth = new AuthModel(req.body);
    Auth.save()
        .then( (user) => {
            console.log('user',user);
            if(!user || user.length === 0)
                return res.status(500).json({flag:false, message:'fail to register ! please try again.'});
            res.status(201).json({flag:true,message:'Congratulation ! user registered successfully'});
        })
        .catch( (err) => {
            console.log('inside error', err);
            res.status(500).json(err);
        })
});

router.post('/login',(req,res)=>{
    if(!req.body.username && !req.body.password)
        return res.status(400).send('Please Provide valid credential !');
    if(!req.body.username || !req.body.password)
        return res.status(400).send('Please Provide username and Password !');
     AuthModel.findOne({
        username:req.body.username,
        password:req.body.password
    }).then((usr) => {
        if(usr === null){
            return res.status(400).send("username/password is incorrect !");
        }
        // generating token for user...
        jwt.sign({user:usr,},config.secret, {expiresIn:'1h'}, (err, token)=>{
            if(err){
                res.status(500).json({message:"fail to generate token !"})
            }
            res.json({
                token,
                message:"success"
            });
        });
    }).catch((err) => {
        return res.status(400).json(err);
    });
} );


module.exports = router;