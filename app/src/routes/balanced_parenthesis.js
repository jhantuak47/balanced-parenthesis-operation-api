const express = require('express');
const Balanced = require('../models/balancedModel');
const router = express.Router();
let myMap = new Map();
// setting the values
myMap.set("{", '}');
myMap.set("(", ')');
myMap.set("[", ']');
var top = -1;
var stack = [];

// get paranthesis..
router.post('/balanced',(req,res) => {
    let user = req.decoded.user;
    if(typeof req.body.parenthesis === 'undefined')
         res.status(400).send('please provide parenthesis !');
    let response = isBalanced(req.body.parenthesis);
    // using response to set message...
    let message = response.flag ? 'success':'';
        let query = {user:user._id};
        let updateOrCreate = {
            $set:{ message },
            $inc:{ attempts: 1}
        };
        let options =  { upsert: true, new: true, setDefaultsOnInsert: true };


        // Balanced.findOneAndUpdate(query,updateOrCreate,options,(err, doc) => {
        //     if(err){
        //         console.log('fail to update balanced', err);
        //     }
        //     if(doc){
        //         console.log('updated doc', doc);
        //     }else{
        //         console.log('null doc', doc);
        //     }
        //
        //     res.json( {response:doc response.msg );
        // });

    Balanced.findOneAndUpdate(query,updateOrCreate,options)
        .populate('user')
        .exec((err, doc) => {
                if(err || doc == null || !doc){
                    console.log('fail to update balanced', err);
                    res.send(response.msg);
                }
                res.json({ response:{
                            username: doc.user.username,
                            message:doc.message,
                            attempts:doc.attempts,
                    },message:response.msg });
            });

});

let isBalanced = (parenthesis = parenthesis.toString()) => {
    for(i=0; i<parenthesis.length; i++){

        if( parenthesis.charAt(i) === '(' || parenthesis.charAt(i) === '[' || parenthesis.charAt(i) === '{')
            push(parenthesis.charAt(i));
        if(parenthesis.charAt(i) === ')' || parenthesis.charAt(i) === ']' || parenthesis.charAt(i) === '}'){
            if(isEmptyStack()){
                for (const [k, v] of myMap) {
                    if(parenthesis.charAt(i) === v)
                        return {flag:false, msg:`unbalanced ${k} is missing.`};
                }
            }

            if( !matchingPairs( pop(), parenthesis.charAt(i) ) ){
                for (const [k, v] of myMap) {
                    if(parenthesis.charAt(i) === v)
                        return {flag:false, msg:`unbalanced ${k} is missing.`};
                }
            }
        }
    }

    //check if stack contains any paranthesis...
    if( !isEmptyStack() ) {
        let ch = pop();
        return {flag:false, msg:`unbalanced ${myMap.get(ch)} is missing.`};
    }
    return {flag:true, msg:'balanced'};
};

let matchingPairs = (char1, char2) => {
    if (char1 === '(' && char2 === ')')
        return true;
    else if (char1 === '{' && char2 === '}' )
        return true;
    else return char1 === '[' && char2 === ']';
};
//stack operations..
let push = (char) => {
    stack[++top] = char;
};
let pop = () => {
    if(top === -1){
        console.log("stack is underflow !");
        return top;
    }else{
        character = stack[top];
        top--;
        return character;
    }
};
let isEmptyStack = () => (top === -1);
module.exports = router;