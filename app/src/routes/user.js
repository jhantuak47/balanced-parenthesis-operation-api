let express = require('express');
let config = require('../config');
let jwt = require('jsonwebtoken');
let router = express.Router();
let AuthModel = require('../models/authModel');

// LIST ALL USER..
router.get('/users', (req,res) => {
    AuthModel.find({ }).then( (user) =>{
        if(user != null)
            res.json({
                user
            });
        res.json({
            success:false,
            msg:'no user found !'
        });
    }).catch( () => {
        res.json({
            success:false,
            msg:'Some internal error, Please try again !'
        })
    })
});

// DELETE USER
router.delete('/user', (req, res) => {
    if(req.decoded.user.role === false)
        res.status(403).send('You are not authorized to delete users pleease login as admin !');
    if(!req.query.email)
        return res.status(400).send('Missing URL parameter: email');
    AuthModel.findOneAndRemove({
        email:req.query.email,
    },function(err, doc){
        if(err){
            res.status(500).send('Error while deleting please try again !');
        }
        res.status(200).json({success:true,msg:`${doc.email} deleted sudccessfully !`});
    }, {
        new:true
    });
});

module.exports = router;