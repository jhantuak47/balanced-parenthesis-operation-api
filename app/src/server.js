const dotenv = require('dotenv');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
let verifyTokens = require('./middlewares/verify_token');
const app = express();
const balanced = require('./routes/balanced_parenthesis');
const user = require('./routes/user');
dotenv.config();

const PORT = process.env.PORT || 5000;

//database connection
const server = process.env.HOST_NAME || 'localhost',
    database = process.env.DATABASE_NAME  || 'rest-api-workshop',
    username = process.env.USER_NAME || '',
    password =  process.env.PASSWORD || '';

mongoose.connect(`mongodb://${username}:${password}@${server}/${database}`, { useNewUrlParser: true });

// Boby parser for post request..
app.use(bodyParser.json());
app.use(express.static('public'));
app.listen(PORT, () => console.log(`server starts on PORT : ${PORT}`));

// User Registration and Login Routes..
app.use('/api/auth', require('./routes/auth'));
// Protected Routes...
app.use(verifyTokens,balanced);
app.use('/api',verifyTokens, user);


