# Balanced Parenthesis Operation API

Balanced Parenthesis Operation API is a node.js example app that determined given parenthesis are balanced or not:

- Takes Parenthesis as Parameters and then passed into the evaluater to evaluate wherter parenthesis are balanced or not.
- Respond back with 'Balanced' if Paranthesis are balanced orther wise 'unbalanced parenthesis missing'
- Save each attempt of hitting the api corresponding user and number of attempts.

## Requirements

* Node 8
* Git
* mongoDB
* Express

## Common setup

Clone the repo and install the dependencies.

git clone https://jhantuak47@bitbucket.org/jhantuak47/balanced-parenthesis-operation-api.git

~$ cd app 

~$ npm install


## Create .env file

    * create .env file in your root directory(app).
    * copy paste the example.env file into your .env file.
    * Specify all your database credentials in your .env file.



## Run Application

Once setup is done head over to package.json file to run the application.

inside terminal

xyz/app ~$ npm run start-watch


## API's End Point.

    1. Api to do registration.

            http://xx.x.x:PORT/api/auth/register

                ->method :- POST
                ->body   :- {
                            	"username":"xyz",
                            	"email":"xyz",
                            	"password":"xyz",
                            	"dob":"xyz",
                            	"role":true/false (true = admin)
                            }

    2. Implement the Login authentication with JWT.

                    http://xx.x.x:PORT/api/auth/login

                    ->method :- POST
                    ->body   :- {
                                   	"username":"xyz",
                                   	"password":"xyz"
                                }

    3. Balanced or unbalanced paranthesis.

                     http://xx.x.x.x:PORT/balanced

                     -> method :- POST

                     -> Headers :- Authorization (

                                        Bearer 'token' <!-- pass token genereted during login 'MANDATORY' -->
                                   )

                     -> body   :- {
                                 	"parenthesis":" ({[]"
                                 }

    4. API to list all the registered users.

                    http://xx.x.x.x:PORT/api/users

                     -> method :- GET

                     -> Headers :- Authorization (

                                        Bearer 'token' <!-- pass token genereted during login 'MANDATORY' -->
                                   )

    5. API to delete the registered user

                    http://xx.x.x.x:PORT/api/users

                     -> method :- GET

                     -> Headers :- Authorization (

                                        Bearer 'token' <!-- pass token genereted during login 'MANDATORY' -->
                                   )

                     -> Query Pramenter :- { "email": "xxxx"} <!-- pass email to delete the user --!>



